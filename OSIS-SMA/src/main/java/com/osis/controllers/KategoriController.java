package com.osis.controllers;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.osis.models.Forum;
import com.osis.services.ForumService;
//import com.osis.services.KategoriService;
//import com.osis.services.PengurusService;
import com.osis.services.UserService;

@Controller
public class KategoriController {
	private ForumService forumService;
	private UserService userService;
//	private PengurusService pengurusService;
//	private KategoriService kategoriService;
	public ForumService getForumService() {
		return forumService;
	}
	@Autowired
	public void setForumService(ForumService forumService) {
		this.forumService = forumService;
	}
	public UserService getUserService() {
		return userService;
	}
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
//	public PengurusService getPengurusService() {
//		return pengurusService;
//	}
//	@Autowired
//	public void setPengurusService(PengurusService pengurusService) {
//		this.pengurusService = pengurusService;
//	}

	@RequestMapping("/forum")
	public String forum(){
		return "form_add_forum";
	} 
	
	@RequestMapping(value="/all/forum", method = RequestMethod.GET)
	public String allForum(Model modal){
		modal.addAttribute("allForum", forumService.getAllForumm());
		return "forum";
	}
	
	@RequestMapping(value="/create/forum", method = RequestMethod.GET)
	public String tampilkanForum(Model model) {
		model.addAttribute("forum", new Forum());
		return "form_add_forum";
	}
	
	@RequestMapping(value="/create/forum", method = RequestMethod.POST)
	public String simpanForum(Model model, Forum forum, HttpServletRequest request){
		model.addAttribute("forum", forumService.save(forum));
		model.addAttribute("alert", true);
		return "form_add_forum";
	}
	
	@RequestMapping(value="/forum/{id}", method = RequestMethod.GET)
	public String lihatForum(@PathVariable Integer id,Model model){
		model.addAttribute("forum", forumService.getByIdForum(id));
		return "forum";
	}
}
