package com.osis.controllers;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import com.osis.dao.SiswaDaoImpl;
import com.osis.models.Siswa;
import com.osis.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class OsisController {
	
	@Autowired
	private SiswaDaoImpl siswaDao;
	@Autowired
	private UserService userServices;
	
	
	
	@RequestMapping("/osis")
	public String osis() {
		return "index_config";
	}
	
	@RequestMapping("/osis_session")
	public String osis_session() {
		return "home";
	}
	

	@RequestMapping(value = "/createSekolah", method = RequestMethod.POST)
	public String createSekolah(Model model, @RequestParam("csvname") String csvName, @RequestParam("nama_sekolah") String namaSekolah){
		System.out.println(csvName);
		userServices.saveFromCSV(csvName);
		model.addAttribute("namaSekolah",namaSekolah);
		
		
		return "index_config";
	}
	
	@RequestMapping(value = "/createSekolah_session", method = RequestMethod.POST)
	public String createSekolah_session(Model model, @RequestParam("csvname") String csvName){
		siswaDao.saveFromCSV(csvName);
		return "redirect:/osis_session";
	}
		
	@RequestMapping(value="/download/{filename:.+}")
	public void getLogFile(HttpServletResponse response, @PathVariable String filename) throws Exception {
	    try {
	        String filePathToBeServed = "D:/CSV/"+filename;
	        File fileToDownload = new File(filePathToBeServed);
	        InputStream inputStream = new FileInputStream(fileToDownload);
	        response.setContentType("application/force-download");
	        response.setHeader("Content-Disposition", "attachment; filename="+filename); 
	        IOUtils.copy(inputStream, response.getOutputStream());
	        response.flushBuffer();
	        inputStream.close();
	    } catch (Exception e){
//	        LOGGER.debug("Request could not be completed at this moment. Please try again.");
	        e.printStackTrace();
	    }

	}

}
