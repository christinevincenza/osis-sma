package com.osis.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.osis.models.Polling;
import com.osis.models.User;
//import com.osis.services.DivisiService;
//import com.osis.services.PengurusService;
import com.osis.services.PollingService;
import com.osis.services.UserService;

@Controller
public class PollingController {
	private PollingService pollingService;
	private UserService userService;
//	private PengurusService pengurusService;
//	private DivisiService divisiService;
	public PollingService getPollingService() {
		return pollingService;
	}
	@Autowired
	public void setpollingService(PollingService pollingService) {
		this.pollingService = pollingService;
	}
	public UserService getUserService() {
		return userService;
	}
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
//	public PengurusService getPengurusService() {
//		return pengurusService;
//	}
//	@Autowired
//	public void setPengurusService(PengurusService pengurusService) {
//		this.pengurusService = pengurusService;
//	}
//	public DivisiService getDivisiService() {
//		return divisiService;
//	}
//	@Autowired
//	public void setDivisiService(DivisiService divisiService) {
//		this.divisiService = divisiService;
//	}
	
	public String getDeskSingkat(String deskripsi){
		String[] singkat = deskripsi.split(" ");
		String kata= "";
		for(int i=0; i<singkat.length; i++){
			if(i==10) break;
			kata+=singkat[i];
			kata+=" ";
		}
		kata+="...";
		return kata;
		
	}
	
	@RequestMapping(value="/form/add_polling", method = RequestMethod.GET)
	public String formPolling(Model model){
		model.addAttribute("polling", new Polling());
		return "polling";
	}
	
	@RequestMapping(value="/do/polling", method = RequestMethod.GET)
	public String ikutPolling(Model model){
		model.addAttribute("polling", new Polling());
		return "ikut_polling";
	}
	

	@RequestMapping(value="/all/polling", method = RequestMethod.GET)
	public String allpolling(Model modal){
		modal.addAttribute("allPolling", pollingService.getAllPollingAccepted());
		return "all_polling";
	}
	
	@RequestMapping(value="/form/add_polling", method = RequestMethod.POST)
	public String createpolling(@RequestParam("images") MultipartFile file,Model model,Polling polling,HttpServletRequest request){
		if (!file.isEmpty()) {
			polling.setFoto(file.getOriginalFilename());
			String name = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
				new FileOutputStream(new File("C:\\Users\\Oriani Sihaloho\\Desktop\\himsi - Copy\\src\\main\\resources\\static\\image\\polling"+name)));
				stream.write(bytes);
				stream.close();
				System.out.println(stream);
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		User user = (User) request.getSession().getAttribute("user");
		polling.setWaktu_publish_polling(dtf.format(now));
//		polling.setPengurus(user.getPengurus());
//		Integer idP = user.getPengurus().getId();
//		if(idP>=1 && idP<=2) {
//			polling.setStatus_polling("accepted");
//		}
//		else{
////			polling.setDivisi(user.getPengurus().getDivisi());
//			polling.setStatus_polling("pending");
//		}
		polling.setIsi_singkat_polling(getDeskSingkat(polling.getIsi_polling()));
		pollingService.saveOrUpdate(polling);
		model.addAttribute("polling", new Polling());
		model.addAttribute("alert", true);
		return "form_polling";
	}
	
	@RequestMapping(value="/polling/{id}", method = RequestMethod.GET)
	public String lihatpolling(@PathVariable Integer id,Model model){
		model.addAttribute("polling", pollingService.getById(id));
		return "polling";
	}
}
