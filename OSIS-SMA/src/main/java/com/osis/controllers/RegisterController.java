package com.osis.controllers;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.tomcat.util.http.fileupload.IOUtils;

import com.osis.dao.RegisterDao;
import com.osis.dao.SiswaDaoImpl;


import com.osis.models.Register;
import com.osis.models.Siswa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegisterController {

	private RegisterDao aDao;
	
	private Register registerRegister;

	@Autowired
	public void setuDao(RegisterDao aDao) {
		this.aDao = aDao;
	}

	@RequestMapping("/konfigurasi")
	public String osis() {
		return "csv1";
	}
	
	
	@RequestMapping("/register")
	public String index(Model model) {
		model.addAttribute("register", new Register());
		
		model.addAttribute("registerRegister",new Register());
		return "register";
	}

	@RequestMapping(value = "/konfigurasi", method = RequestMethod.POST)
	public String saveOrUpdateAkun(Model model, Register register ) {
		model.addAttribute("register", aDao.saveOrUpdate(register));
		model.addAttribute("namaSekolah",register.getNama());
		
		return "csv1";
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request){
		request.getSession().removeAttribute("registerRegister");
		return "redirect:/index";
	}
	

	
	public RegisterDao getaDao() {
		return aDao;
	}
	public void setaDao(RegisterDao aDao) {
		this.aDao = aDao;
	}
	
	public Register getRegisterRegister() {
		return registerRegister;
	}
	public void setRegister(Register registerRegister) {
		this.registerRegister = registerRegister;
	}

}


