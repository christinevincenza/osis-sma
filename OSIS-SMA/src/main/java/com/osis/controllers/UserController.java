package com.osis.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.osis.dao.PasswordDao;
import com.osis.models.Role;
import com.osis.models.User;
import com.osis.services.UserService;

@Controller
public class UserController {
	private UserService userService;
	private PasswordDao passwords;

	public PasswordDao getPassword() {
		return passwords;
	}

	@Autowired
	public void setPassword(PasswordDao password) {
		this.passwords = password;
	}

	public UserService getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "/daftar/siswa", method = RequestMethod.GET)
	public String daftarsiswa(Model model) {
		model.addAttribute("daftarSiswa", userService.getAllUser());
		return "daftar_siswa";
	}

	@RequestMapping(value = "/siswa/lihat/{id}", method = RequestMethod.GET)
	public String lihasiswa(@PathVariable Integer id, Model model) {
		model.addAttribute("siswa", userService.getById(id));
		return "siswa";
	}

	@RequestMapping(value = "/siswa/delete/{id}", method = RequestMethod.GET)
	public String deleteUser(@PathVariable Integer id, Model model) {
		model.addAttribute("daftarsiswa", userService.getAllUser());
		model.addAttribute("alertTrue", true);
		return "daftar_siswa";
	}

	@RequestMapping(value = "/siswa/profile/{id}", method = RequestMethod.GET)
	public String profilUser(@PathVariable Integer id, Model model, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null)
			return "redirect:/login";
		if (user.getId() != id)
			id = user.getId();
		model.addAttribute("siswa", userService.getById(id));
		model.addAttribute("profile", true);
		return "siswa";
	}

	@RequestMapping(value = "/siswa/rolee/{id}", method = RequestMethod.GET)
	public String roleUser(@PathVariable Integer id, Model model, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null)
			return "redirect:/login";
//		if (user.getId() != id)
//			id = user.getId();
		model.addAttribute("siswa", userService.getById(id));
		model.addAttribute("profile", true);
		return "siswa_role";
	}

	@RequestMapping(value = "/edit/siswa/{id}", method = RequestMethod.GET)
	public String editToUser(@PathVariable Integer id, Model model, User siswa, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null)
			return "redirect:/login";
//		if (user.getId() != id)
//			id = user.getId();
		model.addAttribute("siswa", userService.getById(id));
		return "edit_siswa";
	}

	@RequestMapping(value = "/edit/rolee/{id}", method = RequestMethod.GET)
	public String roleToUser(@PathVariable Integer id, Model model, User siswa, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		if (user == null)
			return "redirect:/login";
//		if (user.getId() != id)
//			id = user.getId();
		model.addAttribute("siswa", userService.getById(id));
		return "edit_siswa_role";
	}

	@RequestMapping(value = "/edit/siswa", method = RequestMethod.POST)
	public String editUser(User siswa, HttpServletRequest request) {
		User user = userService.getById(siswa.getId());
		String nama = request.getParameter("nama");
		String alamat = request.getParameter("alamat");
		String no_telepon = request.getParameter("no_telepon");
		String jenis_kelamin = request.getParameter("jenis_kelamin");
		String foto = request.getParameter("foto");
		user.setNama(nama);
		user.setAlamat(alamat);
		user.setNo_telepon(no_telepon);
		user.setJenis_kelamin(jenis_kelamin);
		user.setFoto(foto);
		userService.saveOrUpdate(user);
		return "redirect:/siswa/profile/" + siswa.getId();
	}

	@RequestMapping(value = "/edit/rolee", method = RequestMethod.POST)
	public String editRole(User siswa, HttpServletRequest request) {
		User user = userService.getById(siswa.getId());
		String nama = request.getParameter("nama");
		String alamat = request.getParameter("alamat");
		String no_telepon = request.getParameter("no_telepon");
		String kelas_siswa = request.getParameter("kelas_siswa");
		String deskripsi_role = request.getParameter("deskripsi_role");
		user.setNama(nama);
		user.setAlamat(alamat);
		user.setNo_telepon(no_telepon);
		user.setJenis_kelamin(kelas_siswa);
//		Role.setDeskripsi_role(deskripsi_role);
		userService.saveOrUpdate(user);
		return "redirect:/siswa/rolee/" + user.getId();
	}
	
	@RequestMapping(value = "/edit/password", method = RequestMethod.POST)
	public String editPassword(User siswa, HttpServletRequest request) {
		User user = userService.getById(siswa.getId());
		String password = request.getParameter("password");
		String hashed = passwords.encode(password);
		user.setPassword(hashed);
		userService.saveOrUpdate(user);
		return "redirect:/edit/siswa/" + siswa.getId();
	}

	@RequestMapping(value = "/edit/foto", method = RequestMethod.POST)
	public String createFoto(@RequestParam("foto") MultipartFile file, Model model, User user,
			HttpServletRequest request) {
		if (!file.isEmpty()) {
			user.setFoto(file.getOriginalFilename());
			String foto = file.getOriginalFilename();
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("" + foto)));
				stream.write(bytes);
				stream.close();
				System.out.println(stream);
			} catch (Exception e) {
				return "You failed to upload " + foto + " => " + e.getMessage();
			}
		}

		return "redirect:/edit/siswa/" + user.getFoto();
	}

}
