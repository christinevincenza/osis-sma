package com.osis.controllers;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.osis.models.Berita;
import com.osis.models.User;
import com.osis.services.BeritaService;
import com.osis.services.UserService;

@Controller
public class ValidationController {
	private UserService userService;
	private BeritaService beritaService;
	
	public BeritaService getBeritaService() {
		return beritaService;
	}
	@Autowired
	public void setBeritaService(BeritaService beritaService) {
		this.beritaService = beritaService;
	}
	
	public UserService getUserService() {
		return userService;
	}
	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	@RequestMapping(value="/validation/create", method = RequestMethod.GET)
	public String lihatValidasi(Model modal, HttpServletRequest request){
		modal.addAttribute("allBerita",beritaService.getAllBerita());		
		return "validation";
	}
	
	@RequestMapping(value="/berita/accept/{id}", method = RequestMethod.GET)
	public String acceptBerita(@PathVariable Integer id,Model modal, HttpServletRequest request){
		User user = (User) request.getSession().getAttribute("user");
		Berita berita = beritaService.getById(id);
		berita.setStatus_berita("accepted");
		beritaService.saveOrUpdate(berita);
		return "redirect:/validation/create";
	}
	
	@RequestMapping(value="/berita/reject/{id}", method = RequestMethod.GET)
	public String rejectBerita(@PathVariable Integer id,Model modal, HttpServletRequest request){
		Berita berita = beritaService.getById(id);
		berita.setStatus_berita("rejected");
		beritaService.saveOrUpdate(berita);
		return "redirect:/validation/create";
	}
}
