package com.osis.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.osis.models.Forum;
import com.osis.services.ForumService;


@Service
public class KategoriDao implements ForumService {
	private EntityManagerFactory emf;
	
	@Override
	public Forum save(Forum forum) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Forum saved = em.merge(forum);
		em.getTransaction().commit();
		return saved;
	}
	
	@Override
	public List<Forum> getAllForumByIdKategori(int id) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Forum where id="+id, Forum.class).getResultList();
	}
	
	@Override
	public List<Forum> getAllForumm() {
		EntityManager em = emf.createEntityManager();
		List<Forum> forums = em.createQuery("from Forum",Forum.class).getResultList();
		return forums;
	}

	@Override
	public Forum getByIdForum(int id) {
		EntityManager em = emf.createEntityManager();
		return em.find(Forum.class, id);
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	
	

}
