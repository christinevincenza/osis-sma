package com.osis.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.osis.models.Polling;
import com.osis.services.PollingService;

@Service
public class PollingDao implements PollingService {
	private EntityManagerFactory emf;
	
	@Override
	public Polling saveOrUpdate(Polling Polling) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Polling saved = em.merge(Polling);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public List<Polling> getAllPolling() {
		EntityManager em = emf.createEntityManager();
		List<Polling> Pollings = em.createQuery("from Polling",Polling.class).getResultList();
		return Pollings;
	}

	@Override
	public Polling getById(int id) {
		EntityManager em = emf.createEntityManager();
		return em.find(Polling.class, id);
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public List<Polling> getAllPollingByIdDivisi(int id) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Polling where id="+id, Polling.class).getResultList();
	}

	@Override
	public List<Polling> getAllPollingByStatus(String status) {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from Polling where status_Polling='" + status + "'", Polling.class).getResultList();
	}

	@Override
	public List<Polling> getTop6() {
		List<Polling> Pollings = getAllPollingByStatus("accepted");
		List<Polling> Polling = new ArrayList<>();
		int j=0;
		for(int i=Pollings.size()-1; i>=0; i--){
			Polling.add(Pollings.get(i));
			j++;
			if(j==6) break;
		}
		return Polling;
	}

	@Override
	public List<Polling> getAllPollingAccepted() {
		EntityManager em = emf.createEntityManager();
		List<Polling> Pollings = em.createQuery("from Polling where status_Polling='" + "accepted" + "'",Polling.class).getResultList();
		return Pollings;
	}

	@Override
	public List<Polling> getAllPollingImgTop3() {
		List<Polling> PollingAccepted = getAllPollingAccepted();
		List<Polling> PollingImg = new ArrayList<>();
		int j=0;
		try{
		for(int i=PollingAccepted.size()-1; i>=0; i--){
			System.out.println("->"+PollingAccepted.get(i).getFoto());
			if(PollingAccepted.get(i).getFoto()!=null) PollingImg.add(PollingAccepted.get(i));
			j++;
			if(j==3) break;
		}
		}
		catch(NullPointerException ie){
			return null;
		}
		return PollingImg;
	}
	

}
