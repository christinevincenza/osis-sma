package com.osis.dao;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.opencsv.CSVReader;
import com.osis.models.Register;
import com.osis.models.Siswa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import com.osis.services.RegisterService;

@Service
public class RegisterDao implements RegisterService {
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	
	@Autowired
	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@Override
	public List<Register> getAllRegister() {
		EntityManager em = entityManagerFactory.createEntityManager();
		return em.createQuery("from profil_sekolah",Register.class).getResultList();
	}



	@Override
	public Register saveOrUpdate(Register register) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Register saved = em.merge(register);
		em.getTransaction().commit();
		return saved;
	}

	

}
