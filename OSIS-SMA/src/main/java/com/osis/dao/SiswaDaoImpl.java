package com.osis.dao;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.osis.dao.SiswaDaoImpl;
import com.osis.models.Siswa;
import com.osis.services.SiswaDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;

@Service
public class SiswaDaoImpl implements SiswaDao {
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	@Autowired
    private JavaMailSender emailSender;

	@Override
	public Siswa saveOrUpdate(Siswa siswa) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		Siswa saved = em.merge(siswa);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public void saveFromCSV(String csvName) {	
		
		String CSV_FILE_PATH = "D:/CSV/"+csvName;
        String[] nextRecord;
		
		try {
			Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
	        CSVReader csvReader = new CSVReader(reader);
	        while ((nextRecord = csvReader.readNext()) != null) {
	        	Siswa siswa = new Siswa(0, nextRecord[0], nextRecord[1], nextRecord[2], nextRecord[3]);
	        	
	        	
	        	SimpleMailMessage message = new SimpleMailMessage();
	            message.setSubject("Konfirmasi Akun Sistem Informasi OSIS SMA");
	            message.setText("Silahkan gunakan Akun dibawah ini untuk masuk ke Sistem Informasi OSIS SMA:\nUsername : "+siswa.getNis()+
	            		"\nPassword : "+siswa.getPassword());
	            message.setTo(siswa.getEmail());
	            message.setFrom("no-reply@osis.com");
	            emailSender.send(message);
	            
	            
	        	siswa = saveOrUpdate(siswa);
	        }
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	

}
