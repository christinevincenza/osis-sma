package com.osis.dao;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.osis.dao.PasswordDao;
import com.osis.models.User;
import com.osis.services.RoleService;
import com.osis.services.UserService;

@Service
public class UserDao implements UserService {
	
	

	private EntityManagerFactory emf;
	private PasswordDao passwords;
	@Autowired
    private JavaMailSender emailSender;
	@Autowired
	private RoleService roleService;
	
	
	
	@Override
	public void saveFromCSV(String csvName) {
		String CSV_FILE_PATH = "D:/CSV/"+csvName;
        String[] nextRecord;
        User user = new User();
		try {
			Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
			
	        CSVReader csvReader = new CSVReader(reader);
	       
	        while ((nextRecord = csvReader.readNext()) != null) {
	        
	        	
	        	user.setId_user(nextRecord[0]);
	       
	        	user.setNama(nextRecord[1]);
	       
	        	user.setNis(nextRecord[2]);
	        	
	        	user.setPassword(nextRecord[3]);
	        	
	        	user.setTanggal_lahir(nextRecord[4]);
	        
	        	user.setJenis_kelamin(nextRecord[5]);
	        	user.setAlamat(nextRecord[6]);
	        	user.setEmail(nextRecord[7]);
	        	user.setNo_telepon(nextRecord[8]);
	        	user.setKelas_siswa(nextRecord[9]);
	        	user.setFoto(nextRecord[10]);
	        	user.setRole(roleService.getById(Integer.parseInt(nextRecord[11])));
	        	
	        	
	        	SimpleMailMessage message = new SimpleMailMessage();
	            message.setSubject("Konfirmasi Akun Sistem Informasi OSIS");
	            message.setText("Silahkan gunakan Akun dibawah ini untuk masuk ke Sistem Informasi OSIS:\nUsername : "+user.getId_user()+
	            		"\nPassword : "+user.getPassword());
	            message.setTo(user.getEmail());
	            message.setFrom("no-reply@osis.com");
	            emailSender.send(message);
	            
	            
	        	user = saveOrUpdate(user);
	        }
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public PasswordDao getPassword() {
		return passwords;
	}
	@Autowired
	public void setPassword(PasswordDao password) {
		this.passwords = password;
	}

	@Override
	public List<User> getAllUser() {
		EntityManager em = emf.createEntityManager();
		return em.createQuery("from User", User.class).getResultList();
	}

	@Override
	public User getById(int id) {
		EntityManager em = emf.createEntityManager();
		return em.find(User.class, id);
	}

	@Override
	public User loginUser(String username, String password) {
		List<User> users = getAllUser();
		for(int i=0; i<users.size(); i++){
			if(users.get(i).getId_user().equals(username)){
				if(passwords.matches(password, users.get(i).getPassword())) return users.get(i);
				else return null;
			}
		}
		return null;
	}

	public EntityManagerFactory getEmf() {
		return emf;
	}
	@Autowired
	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

	@Override
	public User saveOrUpdate(User user) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		User saved = em.merge(user);
		em.getTransaction().commit();
		return saved;
	}

	@Override
	public void deleteSiswaById(int id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.remove(em.find(User.class, id));
		System.out.println(findSiswaById(id).getId());
		em.getTransaction().commit();
	}

	@Override
	public User findSiswaById(int id) {
		EntityManager em = emf.createEntityManager();		
		return em.find(User.class, id);
	}

	
	
}
