//package com.osis.models;
//
//import java.util.Set;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
//import javax.persistence.Table;
//import javax.persistence.Version;
//
//@Entity
//@Table(name="t_pengurus")
//public class Pengurus {
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//    private Integer id;
//	private String jabatan;
//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "t_divisi_id")
////    private Divisi divisi;
//	
//	@OneToOne(mappedBy = "t_pengurus",cascade = CascadeType.ALL)
//    private User user;
//	
//	@OneToMany(mappedBy = "t_pengurus")
//    private Set<Berita> beritas;
//	
//	@OneToMany(mappedBy = "t_pengurus")
//    private Set<ProgramKerja> program_kerja;
//	
//	@OneToMany(mappedBy = "t_pengurus")
//    private Set<Pengumuman> pengumumans;
//	
//	
//	@OneToMany(mappedBy = "t_pengurus")
//    private Set<StrukturOrganisasi> struktur_organisasi;
//	
//	
//	@Version
//	@Column(name = "optVersion", columnDefinition="integer DEFAULT 0")
//	private Integer version;
//
//
//	public Integer getId() {
//		return id;
//	}
//
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public String getJabatan() {
//		return jabatan;
//	}
//
//
//	public void setJabatan(String jabatan) {
//		this.jabatan = jabatan;
//	}
//
//
////	public Divisi getDivisi() {
////		return divisi;
////	}
////
////
////	public void setDivisi(Divisi divisi) {
////		this.divisi = divisi;
////	}
//
//
//	public User getUser() {
//		return user;
//	}
//
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//
//	public Set<Berita> getBeritas() {
//		return beritas;
//	}
//
//
//	public void setBeritas(Set<Berita> beritas) {
//		this.beritas = beritas;
//	}
//
//
//	public Set<Pengumuman> getPengumumans() {
//		return pengumumans;
//	}
//
//
//	public void setPengumumans(Set<Pengumuman> pengumumans) {
//		this.pengumumans = pengumumans;
//	}
//
//	public Integer getVersion() {
//		return version;
//	}
//
//
//	public void setVersion(Integer version) {
//		this.version = version;
//	}
//
//
//	public Set<ProgramKerja> getProgram_kerja() {
//		return program_kerja;
//	}
//
//
//	public void setProgram_kerja(Set<ProgramKerja> program_kerja) {
//		this.program_kerja = program_kerja;
//	}
//
//
//	public Set<StrukturOrganisasi> getStruktur_organisasi() {
//		return struktur_organisasi;
//	}
//
//
//	public void setStruktur_organisasi(Set<StrukturOrganisasi> struktur_organisasi) {
//		this.struktur_organisasi = struktur_organisasi;
//	}
//	
//	
//}
