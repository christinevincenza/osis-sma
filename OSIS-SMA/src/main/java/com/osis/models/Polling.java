package com.osis.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="t_polling")
public class Polling {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id_polling;
	@Column(nullable = false)
	private String judul_polling;
	@Column(nullable = false)
	private String isi_polling;
	private String isi_singkat_polling;
	private String waktu_publish_polling;
	private String last_update_polling;
	private String foto;
	private String status_polling;	
//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "t_pengurus_id")
//	private Pengurus pengurus;
//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "t_divisi_id")
//	private Divisi divisi;
//	@ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "t_pengurus_approved_id")
//	private Pengurus pengurusApproval;
	
	
	@Version
	@Column(name = "optVersion", columnDefinition="integer DEFAULT 0")
	private Integer version;
	public Integer getId_polling() {
		return id_polling;
	}
	public void setId_polling(Integer id_polling) {
		this.id_polling = id_polling;
	}
	public String getJudul_polling() {
		return judul_polling;
	}
	public void setJudul_polling(String judul_polling) {
		this.judul_polling = judul_polling;
	}
	public String getIsi_polling() {
		return isi_polling;
	}
	public void setIsi_polling(String isi_polling) {
		this.isi_polling = isi_polling;
	}
	
	public String getIsi_singkat_polling() {
		return isi_singkat_polling;
	}
	public void setIsi_singkat_polling(String isi_singkat_polling) {
		this.isi_singkat_polling = isi_singkat_polling;
	}
	public String getWaktu_publish_polling() {
		return waktu_publish_polling;
	}
	public void setWaktu_publish_polling(String waktu_publish_polling) {
		this.waktu_publish_polling = waktu_publish_polling;
	}
	public String getLast_update_polling() {
		return last_update_polling;
	}
	public void setLast_update_polling(String last_update_polling) {
		this.last_update_polling = last_update_polling;
	}
//	public Divisi getDivisi() {
//		return divisi;
//	}
//	public void setDivisi(Divisi divisi) {
//		this.divisi = divisi;
//	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getStatus_polling() {
		return status_polling;
	}
	public void setStatus_polling(String status_polling) {
		this.status_polling = status_polling;
	}
//	public Pengurus getPengurus() {
//		return pengurus;
//	}
//	public void setPengurus(Pengurus pengurus) {
//		this.pengurus = pengurus;
//	}
//	public Pengurus getPengurusApproval() {
//		return pengurusApproval;
//	}
//	public void setPengurusApproval(Pengurus pengurusApproval) {
//		this.pengurusApproval = pengurusApproval;
//	}
	
	
	
}
