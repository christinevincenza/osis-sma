package com.osis.services;

import java.util.List;

import com.osis.models.Pengumuman;

public interface PengumumanService {
	public Pengumuman saveOrUpdate(Pengumuman pengumuman);
	public List<Pengumuman> getAllPengumuman();
	public List<Pengumuman> getAllPengumumanTop15();	
	public Pengumuman getById(int id);
}
