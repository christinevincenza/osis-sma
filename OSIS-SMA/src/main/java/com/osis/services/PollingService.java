package com.osis.services;

import java.util.List;

import com.osis.models.Polling;

public interface PollingService {
	public Polling saveOrUpdate(Polling Polling);
	public List<Polling> getAllPolling();
	public List<Polling> getAllPollingAccepted();	
	public List<Polling> getAllPollingImgTop3();		
	public List<Polling> getTop6();
	public List<Polling> getAllPollingByIdDivisi(int id);
	public Polling getById(int id);
	public List<Polling> getAllPollingByStatus(String status);
}
