package com.osis.services;

import java.util.List;

import com.osis.models.Register;
import com.osis.models.Siswa;

public interface RegisterService {
	List<Register> getAllRegister();
	Register saveOrUpdate(Register register);

}
