package com.osis.services;

import java.util.List;

import com.osis.models.Role;

public interface RoleService {
	public List<Role> getAllRole();
	public Role getById(int id);
}
