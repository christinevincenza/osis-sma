package com.osis.services;

import com.osis.models.Siswa;

public interface SiswaDao {
	Siswa saveOrUpdate(Siswa siswa);
	void saveFromCSV(String csvName);

}
