package com.osis.services;

import java.util.List;

import com.osis.models.User;

public interface UserService {
	public User saveOrUpdate(User user);
	public List<User> getAllUser();
	public User getById(int id);
	public User loginUser(String username, String password);
	User findSiswaById(int id);
	void deleteSiswaById(int id);
	public void saveFromCSV(String csvName);
}
